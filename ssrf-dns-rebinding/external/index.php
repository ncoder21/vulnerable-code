<?php

require_once('../_helpers/strip.php');

$hostname = $_GET['host'];

// for the sake of demonstrating the vulnerability, here is a
// simple and incomplete IP comparison. Ideally, this would be
// more elaborate, but doesn't matter for the proof of concept.
function isLocal($host) {
  return substr(gethostbyname($host), 0, 3) == '127';
}

if (isLocal($hostname)) {
  echo 'Can\'t make a request to 127.0.0.1.';
} else {
  // This helper code makes it easier to demonstrate the code.
  // It will wait *until* the DNS resolves to a local IP address.
  // In a production environment, the time window between a
  // DNS lookup and the request is often far lower, so you have
  // to be lucky and try a number of times.
  while (!isLocal($hostname)) {
    sleep(1);
  }

  // Here the code assumes $host doesn't resolve to a local IP
  // address. The code itself does *another* DNS resolution to
  // fetch the data, making it vulnerable to a race condition.
  echo shell_exec('curl http://' . $hostname . ':8080/');
}
